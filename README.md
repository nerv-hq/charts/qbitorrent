# qBitorrent + VPN

## Requirements

Need the unsafe sysctl `net.ipv4.conf.all.src_valid_mark=1` to be set on the cluster.

On k3s add the following to `/etc/systemd/system/k3s.service`:

```
ExecStart=/usr/local/bin/k3s \
    server \
      '--kubelet-arg=allowed-unsafe-sysctls=net.ipv4.conf.all.src_valid_mark'
```

reload the daemon and restart k3s:

```bash
systemctl daemon-reload
systemctl restart k3s
```